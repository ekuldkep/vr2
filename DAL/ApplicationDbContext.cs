﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

using Domain;

namespace DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public IDbSet<Topic> Topics { get; set; }
        public IDbSet<TopicReport> TopicReports { get; set; }
        public IDbSet<UserTopicChoice> UserTopicChoices { get; set; }
        public IDbSet<UserMatch> UserMatches { get; set; }
        public IDbSet<UserMessage> UserMessages { get; set; }
        public IDbSet<UserReport> UserReports { get; set; }
        public IDbSet<Country> Countries { get; set; }
        public IDbSet<UserGenderPreferences> UserGenderPreferenceses { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}
