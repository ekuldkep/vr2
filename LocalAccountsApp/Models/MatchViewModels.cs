﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain;
using Domain.Enums;

namespace LocalAccountsApp.Models
{
    public class MatchViewModel
    {
        public int MatchId { get; set; }
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public string Description { get; set; }
        public List<VoteViewModel> Choices { get; set; }
        public bool UserAccept { get; set; }
        public bool MatchAccept { get; set; }
    }

    public class VoteViewModel
    {
        public string Topic { get; set; }
        public Choice Choice { get; set; }

    }

    public class MatchUserViewModel
    {
        public int MatchId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public bool UserAccept { get; set; }
        public bool MatchAccept { get; set; }
    }


}