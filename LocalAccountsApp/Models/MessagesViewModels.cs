﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class MessagesViewModel
    {
        public string Id { get; set; }
        public string Message { get; set; }
    }
}