﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Enums;

namespace LocalAccountsApp.Models
{
    public class TopicsViewModels
    {
    }

    public class VoteTopicGetViewModel
    {
        public int TopicId { get; set; }
        public string Description { get; set; }
    }

    public class VoteTopicPostViewModel
    {
        public int TopicId { get; set; }
        public Choice Choice { get; set; }
    }
}