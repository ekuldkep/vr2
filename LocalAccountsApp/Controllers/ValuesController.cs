﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using DAL;
using Domain;

namespace LocalAccountsApp.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET api/values
        public string Get()
        {
            var userName = this.RequestContext.Principal.Identity.Name;
            string userId = this.RequestContext.Principal.Identity.GetUserId();


            ApplicationUser user = db.Users.Find(userId);

            Topic topic = new Topic()
            {
                Description = "Tere",
                Approved = true,
                CreatedAt = DateTime.Now,
                CreatedById = userId,
            };

            db.Topics.Add(topic);
            db.SaveChanges();


            return String.Format("Hello, {0}.", userId);
        }
    }
}
