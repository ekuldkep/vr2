﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DAL;
using Domain;

namespace LocalAccountsApp.Controllers
{
    public class UserTopicChoicesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/UserTopicChoices
        public IQueryable<UserTopicChoice> GetUserTopicChoices()
        {
            return db.UserTopicChoices;
        }

        // GET: api/UserTopicChoices/5
        [ResponseType(typeof(UserTopicChoice))]
        public IHttpActionResult GetUserTopicChoice(int id)
        {
            UserTopicChoice userTopicChoice = db.UserTopicChoices.Find(id);
            if (userTopicChoice == null)
            {
                return NotFound();
            }

            return Ok(userTopicChoice);
        }

        // PUT: api/UserTopicChoices/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUserTopicChoice(int id, UserTopicChoice userTopicChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userTopicChoice.UserTopicChoiceId)
            {
                return BadRequest();
            }

            db.Entry(userTopicChoice).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserTopicChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserTopicChoices
        [ResponseType(typeof(UserTopicChoice))]
        public IHttpActionResult PostUserTopicChoice(UserTopicChoice userTopicChoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserTopicChoices.Add(userTopicChoice);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userTopicChoice.UserTopicChoiceId }, userTopicChoice);
        }

        // DELETE: api/UserTopicChoices/5
        [ResponseType(typeof(UserTopicChoice))]
        public IHttpActionResult DeleteUserTopicChoice(int id)
        {
            UserTopicChoice userTopicChoice = db.UserTopicChoices.Find(id);
            if (userTopicChoice == null)
            {
                return NotFound();
            }

            db.UserTopicChoices.Remove(userTopicChoice);
            db.SaveChanges();

            return Ok(userTopicChoice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserTopicChoiceExists(int id)
        {
            return db.UserTopicChoices.Count(e => e.UserTopicChoiceId == id) > 0;
        }
    }
}