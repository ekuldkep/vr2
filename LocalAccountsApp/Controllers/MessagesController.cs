﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DAL;
using Domain;
using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity;

namespace LocalAccountsApp.Controllers
{
    [Authorize]
    public class MessagesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Message/{id}?last=
        public async Task<IHttpActionResult> GetMessages(string id, DateTime? last)
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();

            var match = await db.UserMatches.Where(x => x.Active && x.UserAccept && x.MatchAccept && (x.UserId == userId && x.MatchId == id) || (x.UserId == id && x.MatchId == userId)).ToListAsync();
            if (match == null)
            {
                return NotFound();
            }

            var messages = db.UserMessages.Where(x => (x.SenderId == userId && x.ReceiverId == id) ||
                                                      (x.SenderId == id && x.ReceiverId == userId) && x.Created > last)
                .ToListAsync();
            
            return Ok(await messages);
        }

        // Post: api/Messages
        public async Task<IHttpActionResult> PostMessages(MessagesViewModel vm)
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();
            var match = await db.UserMatches.Where(x => x.Active && x.UserAccept && x.MatchAccept && (x.UserId == userId && x.MatchId == vm.Id) || (x.UserId == vm.Id && x.MatchId == userId)).FirstOrDefaultAsync();
            if (match == null)
            {
                return NotFound();
            }

            if (vm.Message.Length == 0 || vm.Message.Length > 255)
            {
                return BadRequest();
            }

            var msg = new UserMessage()
            {
                SenderId = userId,
                ReceiverId = vm.Id,
                Message = vm.Message,
                Created = DateTime.Now
            };

            db.UserMessages.Add(msg);
            await db.SaveChangesAsync();
            
            return Ok();
        }
    }
}
