﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DAL;
using Domain;
using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity;

namespace LocalAccountsApp.Controllers
{
    [Authorize]
    public class MatchController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Match
        public async Task<IHttpActionResult> GetMatch()
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();

            var genders = db.UserGenderPreferenceses.Where(x => x.ApplicationUserId == userId)
                .Select(x => x.Gender)
                .ToList();

            var user = db.Users.Find(userId);

            if (genders.Count <= 0 || user == null || user.PreferenceAgeStart <= 0 || user.PreferenceAgeStart <= 0 || user.PreferenceMatchProtsent < 0)
            {
                return NotFound();
            }

            var previousMatches = db.UserMatches.Where(x => x.UserId == userId)
                .Select(c => c.MatchId)
                .Union(db.UserMatches.Where(x => x.MatchId == userId).Select(c => c.UserId)).ToList();

            var start = new TimeSpan(user.PreferenceAgeStart * 365, 0, 0, 0);
            var stop = new TimeSpan(user.PreferenceAgeStop * 365, 0, 0, 0);


            var matches = await db.UserGenderPreferenceses
                .Include(u => u.ApplicationUser)
                .Where(x => x.ApplicationUserId != userId 
                    && x.Gender == user.Gender 
                    && x.ApplicationUser.CountryId == user.CountryId 
                    && !previousMatches.Contains(x.ApplicationUserId) 
                    && genders.Contains(x.ApplicationUser.Gender) 
                    && SqlFunctions.DateDiff("day", x.ApplicationUser.Birthday, DateTime.Now) >= (user.PreferenceAgeStart * 365)
                    && SqlFunctions.DateDiff("day", x.ApplicationUser.Birthday, DateTime.Now) <= (user.PreferenceAgeStop * 365)
                    )
                .OrderBy(x => Guid.NewGuid())
                .FirstOrDefaultAsync();
                //.ToList();


            // TODO check for hates and likes and match

            if (matches == null)
            {
                return NotFound();
            }

            var umatch = new UserMatch()
            {
                Created = DateTime.Now,
                Active = true,
                UserId = userId,
                UserAccept = false,
                MatchId = matches.ApplicationUserId,
                MatchAccept = false
            };

            db.UserMatches.Add(umatch);
            await db.SaveChangesAsync();

            var choises = new List<VoteViewModel>();
            var uChoises = await db.UserTopicChoices.Include(x => x.Topic).Where(x => x.UserId == matches.ApplicationUserId).ToListAsync();

            foreach (var tocho in uChoises)
            {
                var cho = new VoteViewModel()
                {
                    Topic = tocho.Topic.Description,
                    Choice = tocho.Choice
                };

                choises.Add(cho);
            }

            var vm = new MatchViewModel()
            {
                MatchId = umatch.UserMatchId,
                Name = matches.ApplicationUser.Name,
                Gender = matches.ApplicationUser.Gender,
                Age = (int)(matches.ApplicationUser.Birthday - DateTime.Now).Days/365,
                Description = matches.ApplicationUser.Description,
                Choices = choises,
                UserAccept = umatch.UserAccept,
                MatchAccept = umatch.MatchAccept,
            };
            
            return Ok(vm);
        }

        // GET: api/Match/Active
        [Route("api/Match/Active")]
        public async Task<IHttpActionResult> GetMatchActive()
        {
            var userId = this.RequestContext.Principal.Identity.GetUserId();
            var usermatches = db.UserMatches.Where(x => x.Active && x.UserAccept && x.MatchAccept && (x.UserId == userId || x.MatchId == userId)).ToListAsync();
            var users = new List<MatchUserViewModel>();

            foreach (var mat in await usermatches)
            {
                var ma = new MatchUserViewModel()
                {
                    MatchId = mat.UserMatchId
                };

                if (mat.UserId == userId)
                {
                    var usr = db.Users.Find(mat.MatchId);
                    ma.UserId = usr.Id;
                    ma.Name = usr.Name;
                    ma.UserAccept = ma.UserAccept;
                    ma.MatchAccept = ma.MatchAccept;
                }
                else if (mat.MatchId == userId)
                {
                    var usr = db.Users.Find(mat.UserId);
                    ma.UserId = usr.Id;
                    ma.Name = usr.Name;
                    ma.UserAccept = ma.MatchAccept;
                    ma.MatchAccept = ma.UserAccept;
                }
                else
                {
                    return NotFound();
                }

                users.Add(ma);
            }
            
            return Ok(users);
        }
        // GET: api/Match/Pending
        [Route("api/Match/Pending")]
        public async Task<IHttpActionResult> GetMatchPending()
        {
            var userId = this.RequestContext.Principal.Identity.GetUserId();
            var usermatches = db.UserMatches.Where(x => x.Active && (!x.UserAccept || !x.MatchAccept) && (x.UserId == userId || x.MatchId == userId)).ToListAsync();
            var users = new List<MatchUserViewModel>();

            foreach (var mat in await usermatches)
            {
                var ma = new MatchUserViewModel()
                {
                    MatchId = mat.UserMatchId
                };

                if (mat.UserId == userId)
                {
                    var usr = db.Users.Find(mat.MatchId);
                    ma.UserId = usr.Id;
                    ma.Name = usr.Name;
                    ma.UserAccept = ma.UserAccept;
                    ma.MatchAccept = ma.MatchAccept;
                }
                else if (mat.MatchId == userId)
                {
                    var usr = db.Users.Find(mat.UserId);
                    ma.UserId = usr.Id;
                    ma.Name = usr.Name;
                    ma.UserAccept = ma.MatchAccept;
                    ma.MatchAccept = ma.UserAccept;
                }
                else
                {
                    return NotFound();
                }

                users.Add(ma);
            }
            
            return Ok(users);
        }

        // Post: api/Match
        [HttpPut]
        public async Task<IHttpActionResult> PutMatch(int id)
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();
            var match = db.UserMatches.Find(id);

            if (match == null)
            {
                return NotFound();
            }
            if (match.UserId == userId)
            {
                match.UserAccept = true;
            }
            else if (match.MatchId == userId)
            {
                match.MatchAccept = true;
            }
            else
            {
                return NotFound();
            }

            db.UserMatches.AddOrUpdate(match);
            await db.SaveChangesAsync();

            return Ok();
        }

        // Delete: api/Match
        public async Task<IHttpActionResult> DeleteMatch(int id)
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();
            var match = db.UserMatches.Find(id);

            if (match == null)
            {
                return NotFound();
            }
            if (match.UserId == userId || match.MatchId == userId)
            {
                match.Active = false;
            }
            else
            {
                return NotFound();
            }

            db.UserMatches.AddOrUpdate(match);
            await db.SaveChangesAsync();

            return Ok();
        }
    }
}
