﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DAL;
using Domain;
using Domain.Enums;
using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity;

namespace LocalAccountsApp.Controllers
{
    [Authorize]
    public class TopicsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Topics
        public IQueryable<Topic> GetTopics()
        {
            return db.Topics;
        }

        // GET: api/Topics/5
        [ResponseType(typeof(Topic))]
        public IHttpActionResult GetTopic(int id)
        {
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return NotFound();
            }

            return Ok(topic);
        }

        // PUT: api/Topics/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutTopic(int id, Topic topic)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != topic.TopicId)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(topic).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TopicExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        // POST: api/Topics
        [ResponseType(typeof(Topic))]
        public IHttpActionResult PostTopic(Topic topic)
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();

            if (String.IsNullOrEmpty(userId))
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            if (String.IsNullOrEmpty(topic.Description) || topic.Description.Length > 255)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Topic tp = new Topic()
            {
                Description = topic.Description,
                CreatedById = userId,
                CreatedAt = DateTime.Now,
                //TODO
                Approved = true
            };

            db.Topics.Add(tp);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tp.TopicId }, tp);
        }

        // DELETE: api/Topics/5
        [ResponseType(typeof(Topic))]
        public IHttpActionResult DeleteTopic(int id)
        {
            var isinrole = this.RequestContext.Principal.IsInRole("Admin");

            if (!isinrole)
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }

            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return NotFound();
            }

            db.Topics.Remove(topic);
            db.SaveChanges();

            return Ok(topic);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TopicExists(int id)
        {
            return db.Topics.Count(e => e.TopicId == id) > 0;
        }

        [Route("api/Vote")]
        [ResponseType(typeof(VoteTopicGetViewModel))]
        public async Task<IHttpActionResult> GetVote()
        {
            string userId = this.RequestContext.Principal.Identity.GetUserId();
            var voted = await db.UserTopicChoices.Where(p => p.UserId == userId).Select(p => p.TopicId).ToListAsync();
            var reported = await db.TopicReports.Where(p => p.UserId == userId).Select(p => p.TopicId).ToListAsync();
            var noShow = voted.Concat(reported).ToList();

            var tp = await db.Topics.Where(p => p.Approved && !noShow.Contains(p.TopicId))
                .OrderBy(x => Guid.NewGuid())
                .FirstOrDefaultAsync();

            if (tp == null)
            {
                return NotFound();
            }

            var vm = new VoteTopicGetViewModel()
            {
                TopicId = tp.TopicId,
                Description = tp.Description
            };
            return Ok(vm);
        }


        [Route("api/Vote")]
        [ResponseType(typeof(VoteTopicGetViewModel))]
        public async Task<IHttpActionResult> PostVote(VoteTopicPostViewModel vm)
        {
            if (vm.TopicId <= 0 || !Enum.IsDefined(typeof(Choice), vm.Choice))
            {
                return NotFound();
            }

            string userId = this.RequestContext.Principal.Identity.GetUserId();
            var voted = await db.UserTopicChoices.Where(p => p.UserId == userId).Select(p => p.TopicId).ToListAsync();
            var reported = await db.TopicReports.Where(p => p.UserId == userId).Select(p => p.TopicId).ToListAsync();
            var noShow = voted.Concat(reported).ToList();

            var tp = await db.Topics.Where(p => p.Approved && !noShow.Contains(p.TopicId))
                .Select(p => p.TopicId)
                .ToListAsync();
                
            if (tp == null)
            {
                return NotFound();
            }

            if (!tp.Contains(vm.TopicId))
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var choice = new UserTopicChoice()
                {
                    UserId = userId,
                    TopicId = vm.TopicId,
                    Choice = vm.Choice,
                    CreatedAt = DateTime.Now
                };

                db.UserTopicChoices.Add(choice);
                await db.SaveChangesAsync();
            }

            return Ok();
        }

    }
}