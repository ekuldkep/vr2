﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
   public class UserReport
    {
        public int UserReportId { get; set; }
        public string ReporterId { get; set; }
        //[ForeignKey(nameof(ReporterId))]
        //public ApplicationUser Reporter { get; set; }
        public string ReportedId { get; set; }
        //[ForeignKey(nameof(ReportedId))]
        //public ApplicationUser Reported { get; set; }
        public bool Active { get; set; }
        public DateTime Created { get; set; }
        public string Reason { get; set; }
        public string Verdict { get; set; }

    }
}
