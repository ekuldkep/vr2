﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Topic
    {
        public int TopicId { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Approved { get; set; }
        [Required]
        public string CreatedById { get; set; }

        //[ForeignKey(name: nameof(CreatedById))]
        //public ApplicationUser CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }
        public List<UserTopicChoice> UserTopicChoices { get; set; }
        public List<TopicReport> TopicReports { get; set; }
    }
}
