﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserMessage
    {

        public int UserMessageId { get; set; }
        [Required]
        public string SenderId { get; set; }
        //[ForeignKey(nameof(SenderId))]
        //public ApplicationUser Sender { get; set; }
        [Required]
        public string ReceiverId { get; set; }
        //[ForeignKey(nameof(ReceiverId))]
        //public ApplicationUser Receiver { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }


    }
}
