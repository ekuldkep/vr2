﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class TopicReport
    {
        public int TopicReportId { get; set; }
        public string UserId { get; set; }
        //[ForeignKey(name: nameof(UserId))]
        //public virtual ApplicationUser User { get; set; }
        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Reason { get; set; }
        public string Verdict { get; set; }
        public bool Active { get; set; }
    }
}
