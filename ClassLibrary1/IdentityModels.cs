﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Domain.Enums;

namespace Domain
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        public string Description { get; set; }
        public Gender Gender { get; set; }
        public DateTime? LastActive { get; set; }
        public State State { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int PreferenceAgeStart { get; set; }
        public int PreferenceAgeStop { get; set; }
        public float PreferenceRadius { get; set; }
        public float PreferenceMatchProtsent { get; set; }
        public List<Topic> Topics { get; set; }
        public List<UserTopicChoice> UserTopicChoices { get; set; }
        public List<TopicReport> TopicReports { get; set; }
        //[InverseProperty(nameof(UserMatch.User))]
        //public List<UserMatch> UserInUserMatches { get; set; }
        //[InverseProperty(nameof(UserMatch.Match))]
        //public List<UserMatch> MatchInUserMatches { get; set; }
        //[InverseProperty(nameof(UserMessage.Sender))]
        //public List<UserMessage> UserMessageSender { get; set; }
        //[InverseProperty(nameof(UserMessage.Receiver))]
        //public List<UserMessage> UserMessageReciever { get; set; }
        //[InverseProperty(nameof(UserReport.Reporter))]
        //public List<UserReport> UserReportReporter { get; set; }
        //[InverseProperty(nameof(UserReport.Reported))]
        public List<UserReport> UserReportReported { get; set; }
        public List<UserGenderPreferences> UserGenderPreferenceses { get; set; }

        //public int Age()
        //{
        //    DateTime n = DateTime.Now;
        //    int age = n.Year - Birthday.Year;

        //    if (n.Month < Birthday.Month || (n.Month == Birthday.Month && n.Day < Birthday.Day))
        //        age--;

        //    return age;
        //}

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}