﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserMatch
    {
        public int UserMatchId { get; set; }
        public DateTime Created { get; set; }
        public bool Active { get; set; }
        [Required]
        public string UserId { get; set; }
        //[ForeignKey(nameof(UserId))]
        //public virtual ApplicationUser User { get; set; }
        public bool UserAccept { get; set; }
        [Required]
        public string MatchId { get; set; }
        //[ForeignKey(nameof(MatchId))]
        //public virtual ApplicationUser Match { get; set; }
        public bool MatchAccept { get; set; }
    }
}
