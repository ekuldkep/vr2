﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Enums;
using Domain;

namespace Domain
{
    public class UserTopicChoice
    {
        public int UserTopicChoiceId { get; set; }
        [Required]
        public string UserId { get; set; }
        //[ForeignKey(name: nameof(UserId))]
        //public ApplicationUser User { get; set; }

        [Required]
        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public Choice Choice { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
